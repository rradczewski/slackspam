import { RTMClient } from "@slack/rtm-api";
import { WebClient } from "@slack/web-api";
import debug from "debug";
import { TeamJoinedEvent, User } from "./events";

const LOG = debug("bot");

const token = process.env.SLACK_TOKEN as string;
const adminChannel = process.env.SLACK_LOG_CHANNEL as string;
let adminChannelId;

const webClient = new WebClient(token);
const rtm = new RTMClient(token);
rtm.start().catch(console.error);

const resolveChannelId = async (channelName: string): Promise<string> => {
  let cursor: string | undefined = undefined;
  do {
    const args = !!cursor
      ? { cursor, types: "public_channel,private_channel" }
      : { types: "public_channel,private_channel" };
    const allChannels = await webClient.conversations.list(args);
    const channel = allChannels.channels.find(
      (channel) => channel.name === channelName
    );
    if (channel) return channel.id;

    cursor = allChannels.response_metadata?.next_cursor;
  } while (!!cursor);
  throw new Error("Could not resolve channel " + channelName);
};

rtm.on("ready", async () => {
  LOG("Ready %o");
});

let newUsers: any[] = [];
let notifiedMessages: any[] = [];

rtm.on("reaction_added", async (event: any) => {
  LOG("%o", event);
  if (event.item.type != "message") {
    return;
  }

  if (notifiedMessages.includes(event.item.ts)) {
    LOG("Message %o was already processed", event);
    return;
  }

  const reactions = await webClient.reactions.get({
    channel: event.item.channel,
    timestamp: event.item.ts,
  });
  if (!reactions.ok) {
    LOG("ERROR: %o", reactions);
    return;
  }
  LOG("Got %o (%o)", reactions, reactions.message?.reactions);

  const spamReactions = reactions.message?.reactions?.find(
    (reaction) => reaction.name == "special_spam"
  );

  if (!spamReactions || !spamReactions.count) return;

  if (spamReactions.count >= 3) {
    notifiedMessages.push(event.item.ts);
    
    const link = await webClient.chat.getPermalink({
      channel: event.item.channel,
      message_ts: event.item.ts,
    });
    await webClient.chat.postMessage({
      channel: process.env.SLACK_LOG_CHANNEL as string,
      mrkdwn: true,
      text: `The following message by <@${
        reactions.message?.user
      }> was highlighted to be spam by at least 3 users: ${
        (link as any).permalink
      }`,
      unfurl_links: true,
    });
  }
});

rtm.on("team_join", async (event: TeamJoinedEvent) => {
  LOG("User joined %o", event);

  const user: any = (await webClient.users.info({ user: event.user.id })).user;
  newUsers.push(event);

  webClient.chat.postMessage({
    channel: process.env.SLACK_LOG_CHANNEL as string,
    mrkdwn: true,
    unfurl_links: true,
    text: `*New User*: ${user.name} (${user.profile?.real_name} | <@${user.id}>): Title=${user.profile?.title}, Email=${user.profile?.email}, CustomAvatar=${user.profile?.is_custom_image}`,
  });
});

rtm.on("user_change", async (event: any) => {
  LOG("User changed %o", event);
  if (event.user.deleted) {
    const user: any = (await webClient.users.info({ user: event.user.id }))
      .user;
    webClient.chat.postMessage({
      channel: process.env.SLACK_LOG_CHANNEL as string,
      mrkdwn: true,
      unfurl_links: true,
      text: `*User deactivated*: ${user.name} (${user.profile?.real_name} | <@${user.id}>)`,
    });
  }
});

rtm.on("message", async (event) => {
  LOG("Message posted %j", event);
  if (event.subtype) return;

  if (!newUsers.some((user) => user.user?.id === event.user)) return;
  const user = newUsers.find((user) => user.user?.id === event.user)
    .user as User;

  const containsLink = event.blocks?.some((block) =>
    block.elements?.some((blockElem) =>
      blockElem.elements?.some((blockElemElem) => blockElemElem.type === "link")
    )
  );

  if (containsLink) {
    const link = await webClient.chat.getPermalink({
      channel: event.channel,
      message_ts: event.ts,
    });
    await webClient.chat.postMessage({
      channel: process.env.SLACK_LOG_CHANNEL as string,
      mrkdwn: true,
      text: `User <@${
        event.user
      }> just posted their first message and it contains a link: ${
        (link as any).permalink
      }`,
      unfurl_links: true,
    });
  }
  newUsers = newUsers.filter((user) => user.user?.id !== event.user);
});
