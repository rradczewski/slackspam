FROM node:erbium-alpine AS builder

WORKDIR /opt/app

COPY package.json package-lock.json ./
RUN ["npm", "ci"]

COPY tsconfig.json ./
COPY src ./src
RUN ["npm", "run", "build"]
RUN ["npm", "prune", "--production"]

FROM node:erbium-alpine

WORKDIR /opt/app

COPY --from=builder /opt/app /opt/app

VOLUME ["/opt/app/data"]

CMD ["npm", "run", "forever"]
